<?php

namespace App\Form;

use App\Entity\Invoice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class InvoiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', null, [
                'label' => 'Numer faktury',
                'constraints' => [
                    new NotBlank()
                ]])
            ->add('seller', SellerType::class, ['label' => 'Sprzedający'])
            ->add('buyer', BuyerType::class, ['label' => 'Kupujący'])
            ->add('item', ItemType::class, ['label' => 'Towar']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Invoice::class,
        ]);
    }
}
