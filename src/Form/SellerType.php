<?php

namespace App\Form;

use App\Entity\Seller;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class SellerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Nazwa',
                'constraints' => [
                    new NotBlank()
                ]])
            ->add('nip', null, [
                'label' => 'NIP',
                'constraints' => [
                    new NotBlank()
                ]])
            ->add('street', null, [
                'label' => 'Ulica',
                'constraints' => [
                    new NotBlank()
                ]])
            ->add('city', null, [
                'label' => 'Miejscowość',
                'constraints' => [
                    new NotBlank()
                ]])
            ->add('zipcode', null, [
                'label' => 'Kod pocztowy',
                'constraints' => [
                    new NotBlank()
                ]]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Seller::class,
        ]);
    }
}
