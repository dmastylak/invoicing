<?php

namespace App\Form;

use App\Entity\Item;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Nazwa towaru',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('measure', null, [
                'label' => 'Jednostka',
                'constraints' => [
                    new NotBlank()
                ]])
            ->add('quantity', null, [
                'label' => 'Ilość',
                'constraints' => [
                    new NotBlank()
                ]]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Item::class,
        ]);
    }
}
