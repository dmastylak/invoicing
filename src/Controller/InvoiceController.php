<?php

namespace App\Controller;

use App\Entity\Invoice;
use App\Form\InvoiceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class InvoiceController extends AbstractController
{
    /**
     * @Route("/faktura", name="invoice")
     */
    public function index(Request $request)
    {
        $seller = new Invoice();
        $form = $this->createForm(InvoiceType::class, $seller);

        $form->handleRequest($request);

        $entityManager = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($form->getData());
            $entityManager->flush();

            return $this->redirectToRoute('list');
        } else {
            return $this->render('invoice/index.html.twig', [
                'form' => $form->createView(),
                'controller_name' => 'InvoiceController'
            ]);
        }

        return $this->render('invoice/index.html.twig', [
            'form' => $form->createView(),
            'controller_name' => 'InvoiceController'
        ]);
    }

    /**
     * @Route("/lista", name="list")
     */
    public function list()
    {
        $invoices = $this->getDoctrine()
            ->getRepository(Invoice::class);

        return $this->render('invoice/list.html.twig', [
            'invoices' => $invoices->findAll(),
            'controller_name' => 'InvoiceController'
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository(Invoice::class)->find($id);
        $invoices = $this->getDoctrine()
            ->getRepository(Invoice::class);

        if ($user) {
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('list');
    }
}
